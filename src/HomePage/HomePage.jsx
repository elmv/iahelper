import React, { useEffect, useState } from "react";
import { Link, useLocation } from 'react-router-dom';
import { getCurrentTab } from "../chrome/utils";

export const Home = () => {

    const changeColor = (e) => {
        // console.log(e.currentTarget.id);
        // window.chrome.tabs.executeScript(null, { code: "document.getElementsByClassName('cpfc__item')" });
        // chrome.tabs.executeScript(tabs[tab].id, {
        //     "code": "document.getElementsByClassName('cpfc__item')"
        // }, function (result) {
        //     console.log(result);
        // });
    }

    const sendRemoveMessage = () => {

        getCurrentTab((tab) => {
            console.log(tab);
            tab && window.chrome.tabs.sendMessage(
                tab.id,
                { action: "getFields" },
                function (response) {
                    console.log(response);
                });
        });
    };
    
    const setFields = () => {

        getCurrentTab((tab) => {
            console.log(tab);
            tab && window.chrome.tabs.sendMessage(
                tab.id,
                { action: "setFields" },
                function (response) {
                    console.log(response);
                });
        });
    };

    return (
        <div className="App index">
            <header className="container">
                <section className="serach-section">
                    <input className="input-line" type="text" placeholder="Поиск по ФИО или VIN" name="search" />
                </section>
                <section className="clients-cards">
                    {Array.apply(0, Array(3)).map((x, i) =>
                        <div onClick={setFields} key={i} id={i} className="cc-item">
                            <div className="cci-name">
                                <div className="ccin-text">
                                    Макаров Олег Витальевич
                                </div>
                                <div className="cci-actions">
                                    <div className="ccia-edit"></div>
                                    <div className="ccia-remove"></div>
                                </div>
                            </div>
                            <div className="cci-auto">
                                Mercedes-Benz CL-Класс 500 III
                            </div>
                            <div className="cci-params">
                                <div className="ccip-plate">В780ХВ24</div>
                                <div className="ccip-drivers">2</div>
                                <div className="ccip-pts">KL•••31</div>
                            </div>
                        </div>
                    )}
                </section>
                <section className="copy-section">
                    <div className="cs-actions">
                        <div className="csa-new">+</div>
                        <div className="csa-settings"></div>
                    </div>
                    <div onClick={sendRemoveMessage} className="cs-button">
                        <div className="cs-copiimg"></div>
                        <div className="cs-title">Копировать с сайта</div>
                        <div className="cs-text">Создать карточку на основе данных <br /> из формы сайта</div>
                    </div>
                </section>
                {/* <p>Home</p>
                <p>URL:</p>
                <p>
                    {url}
                </p>
                <button onClick={sendTestMessage}>SEND MESSAGE</button>
                <button onClick={sendRemoveMessage}>Remove logo</button>
                <p>Response from content:</p>
                <p>
                    {responseFromContent}
                </p>
                <button onClick={() => {
                    push('/about')
                }}>About page
                </button> */}
            </header>
        </div>
    )
}
