import { authHeader } from '../_helpers';
import axios from 'axios';

const apiUrl = 'https://iahelper.elwe.xyz';

const login = (email, password) => {
    const requestOptions = {
        method: 'POST',
        url: `${apiUrl}/users/authenticate`,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password })
    };

    return axios(requestOptions);
    // return fetch(`${apiUrl}/users/authenticate`, requestOptions)
    //     .then(handleResponse)
    //     .then(user => {
    //         // store user details and jwt token in local storage to keep user logged in between page refreshes
    //         localStorage.setItem('user', JSON.stringify(user));

    //         return user;
    //     });
}

const logout = () => {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

const me = () => {
    const requestOptions = {
        method: 'GET',
        url: `${apiUrl}/me`,
        headers: authHeader()
    };

    return axios(requestOptions);
}

const getById = (id) => {
    const requestOptions = {
        method: 'GET',
        url: `${apiUrl}/users/${id}`,
        headers: authHeader()
    };

    // return fetch(`${apiUrl}/users/${id}`, requestOptions).then(handleResponse);
    return axios(requestOptions);
}

const register = (user) => {
    const requestOptions = {
        method: 'POST',
        url: `${apiUrl}/user/register`,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return axios(requestOptions);
}

const update = (user) => {
    const requestOptions = {
        method: 'PUT',
        url: `${apiUrl}/user/${user.id}`,
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return axios(requestOptions);
}

// prefixed =  const name with underscore because delete is a reserved word in javascrip=> t
// const =  _delete(id) => {
//     const requestOptions = {
//         method: 'DELETE',
//         headers: authHeader()
//     };

//     return fetch(`${apiUrl}/users/${id}`, requestOptions).then(handleResponse);
// }

// const =  handleResponse(response) => {
//     return response.text().then(text => {
//         const data = text && JSON.parse(text);
//         if (!response.ok) {
//             if (response.status === 401) {
//                 // auto logout if 401 response returned from api
//                 // logout();
//                 // window.location.reload(true);
//             }

//             const error = (data && data.message) || response.statusText;
//             return Promise.reject(error);
//         }

//         return data;
//     });
// }

export const userService = {
    login,
    logout,
    register,
    me,
    getById,
    update,
    // delete: _delete
};