import { userConstants } from '../_constants';
import { userService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

const login = (email, password, from) => {
    return dispatch => {
        dispatch(request());
        userService.login(email, password)
            .then(
                user => {
                    // localStorage.setItem('user', JSON.stringify(user));
                    dispatch(success(user));
                    history.push(from);
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(){ return { type: userConstants.LOGIN_REQUEST } }
    function success(user){ return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error){ return { type: userConstants.LOGIN_FAILURE, error } }
}

const logout = () => {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

const register = (user) => {
    return dispatch => {
        dispatch(request(user));

        userService.register(user)
            .then(
                user => {
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user){ return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user){ return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error){ return { type: userConstants.REGISTER_FAILURE, error } }
}

const me = () => {
    return dispatch => {
        dispatch(request());

        userService.me()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request(){ return { type: userConstants.me_REQUEST } }
    function success(users){ return { type: userConstants.me_SUCCESS, users } }
    function failure(error){ return { type: userConstants.me_FAILURE, error } }
}

// prefixed const name =  with underscore because delete is a reserved word in javascript
// const _delete = (id) => {
//     return dispatch => {
//         dispatch(request(id));

//         userService.delete(id)
//             .then(
//                 user => dispatch(success(id)),
//                 error => dispatch(failure(id, error.toString()))
//             );
//     };

//     function request(id) => { return { type: userConstants.DELETE_REQUEST, id } }
//     const success = (id) => { return { type: userConstants.DELETE_SUCCESS, id } }
//     const failure = (id, error) => { return { type: userConstants.DELETE_FAILURE, id, error } }
// }

export const userActions = {
    login,
    logout,
    register,
    me,
    // delete: _delete
};