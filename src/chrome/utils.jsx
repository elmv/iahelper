export const getCurrentTab = (callback) => {
    const queryInfo = { active: true, currentWindow: true };

    window.chrome.tabs.query(queryInfo, tabs => {
        const currTab = tabs[0];
        if (currTab) {
            callback(currTab);
        }
    });
}
