// const validateSender = (message, sender) => {
//     return sender.id === chrome.runtime.id && message.from === Sender.React;
// }

const messagesFromReactAppListener = (request, sender, sendResponse) => {

    // const isValidated = validateSender(message, sender);
    console.log(request);
    console.log(sender);
    // sendResponse({farewell: "goodbye"});
    // console.log(sender.tab ?
    //     "from a content script:" + sender.tab.url :
    //     "from the extension");
    // if (request.greeting === "hello") {
    //     sendResponse({farewell: "goodbye"});
    // }
    // if (message.message === 'Hello from React') {
    //     // response('Hello from content.js');
    // }

    if (request.action === "delete") {
        const logo = document.getElementById('root');
        logo.parentElement.removeChild(logo)
        sendResponse({msg: 'del'});
    }
    if (request.action === "getFields") {
        const fio = document.getElementById('InsurerPersonName');
        sendResponse({msg: fio.value});
    }
    if (request.action === "setFields") {
        const fio = "фио";
        const childiFrame = document.getElementById("mainPH");
        const innerDoc = childiFrame.contentDocument || childiFrame.contentWindow.document; 
        const input = innerDoc.getElementById("OwnerCalcPersonName");
        input.value = fio;
        input.focus();

        const input2 = innerDoc.getElementById("OwnerCalcPersonbirthDate");
        input2.value = '06.01.2022';
        input2.classList.remove('x-form-empty-field', 'x-form-invalid')

        const input3 = innerDoc.getElementById("AdressSubjectUsageRegion");
        input3.value = 'Алтай Респ';
        input3.classList.remove('x-form-empty-field', 'x-form-invalid')

        const input4 = innerDoc.getElementById("AdressCityUsageRegion");
        input4.value = 'Горно-Алтайск г';
        input4.classList.remove('x-form-empty-field', 'x-form-invalid')

        sendResponse({msg: ''});
    }
}

const main = () => {
    console.log('[content.ts] Main')
    /**
     * Fired when a message is sent from either an extension process or a content script.
     */
    window.chrome.runtime.onMessage.addListener(messagesFromReactAppListener);
}

main();


